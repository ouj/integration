#include "utils.h"
#include <common/morton.h>

vec2i projectToCamera(const vec3f &wp, const Camera* camera, int width, int height) {
    vec3f lp = transformPointInverse(camera->transform, wp);
    vec3f d = normalize(lp);
    if (camera->type == PerspectiveCamera::TYPEID) {
        PerspectiveCamera* lens = (PerspectiveCamera*)camera;
        float a = lens->distance / d.z;
        float x = d.x * a;
        float y = d.y * a;
        vec2f sp = vec2f(x / lens->width * 2.0f,  y / lens->height * 2.0f);
        sp = (sp + one2f) * 0.5f;
        vec2i px = vec2i(sp.x * width, sp.y * height);
        return px;
    } else error("unknown camera type");
    return zero2i;
}

std::vector<range2i> makeImageBlocks(int width, int height, int blocksize) {
    std::vector<range2i> imageBlocks;
    int bWidth = (int)ceil((float)width / blocksize);
    int bHeight = (int)ceil((float)height / blocksize);
    for (int j = 0; j < bHeight; j++) {
        for (int i = 0; i < bWidth; i++) {
            imageBlocks.emplace_back(vec2i(i*blocksize, j*blocksize),
                                     vec2i(min((i+1)*blocksize, width),
                                           min((j+1)*blocksize, height)));
        }
    }
    sort(imageBlocks.begin(), imageBlocks.end(),
         [blocksize](const range2i &b1, const range2i &b2) -> bool{
             uint c1 = morton2d::encode(b1.min.x / blocksize, b1.min.y / blocksize);
             uint c2 = morton2d::encode(b2.min.x / blocksize, b2.min.y / blocksize);
             return c1 < c2;
         });
    return imageBlocks;
}