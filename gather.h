#ifndef _RENDER_GATHER_H_
#define _RENDER_GATHER_H_
#include <common/vec.h>
#include <common/frame.h>
#include "sampling.h"

struct GatherPoint {
    vec3f               le;
    vec3f               wo;
    frame3f             f;
    ReflectanceSample   rs;
    float               weight;
    vec2i               pixel;
};

#endif