#ifndef TRACE_OPTIONS_H
#define TRACE_OPTIONS_H

namespace options {
    namespace trace {        
        const bool envmapsampling = true;
        const bool brdfsampling = true;
        
        const bool checkimage = false;
        
        const int maxsamples = 4096;
        const int maxdimensions = 1024;
        
        const int rngskipblock = 40000*1000;
    }
    
    namespace vpl {
        const bool russianroulette = true;
    }
}

#endif
