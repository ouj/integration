#include "sampling.h"
#include <common/geom.h>
#include <scene/camera.h>
#include <scene/transform.h>
#include <scene/light.h>
#include <scene/shape.h>
#include <scene/emission.h>
#include <scene/options.h>
#include <accelerator/accelerator.h>

// Sampling camera
ray3f sampleCamera(const Camera* camera, const vec2f& is) {
    ray3f ray;
    switch(camera->type) {
        case PerspectiveCamera::TYPEID: {
            PerspectiveCamera* cam = (PerspectiveCamera*)camera;
            vec3f lp = zero3f;
            vec3f ip = x3f*((is.x-0.5f)*cam->width)+
                       y3f*((is.y-0.5f)*cam->height)+
                       z3f*(cam->distance);
            ray = ray3f(lp,normalize(ip-lp));
        } break;
        case OrthogonalCamera::TYPEID: {
            OrthogonalCamera *cam = (OrthogonalCamera*)camera;
            vec3f ip = x3f * (is.x - 0.5f) * cam->width +
                       y3f * (is.y - 0.5f) * cam->height;
            ray = ray3f(ip, z3f);
        } break;
        case PanoramicCamera::TYPEID: {
            float phi = is.x * consts<float>::twopi;
            float theta = (1-is.y) * consts<float>::pi;
            float s = sin(theta); float c = cos(theta);
            vec3f d = vec3f(s * cos(phi), c, s * sin(phi));
            ray = ray3f(zero3f, d);
        } break;
        case FisheyeCamera::TYPEID: {
            vec2f v = (is - half2f) * 2.0f;
            float r2 = v.lengthSqr();
            float r = sqrt(r2);
            float sinp = v.x / r;
            float cosp = v.y / r;
            float sint = min(1.0f, r);
            float cost = max(0.0f, sqrtf(1 - r2));
            vec3f d = vec3f(sinp*sint, cosp*sint, cost);
            if (r2 >= 1.0f) ray = ray3f(zero3f, d, ray3f::infinity, ray3f::epsilon);
            else ray = ray3f(zero3f, normalize(d));
        } break;
        default: error("Unknown camera type."); return ray3f(zero3f,z3f);
    }
    return transformRay(camera->transform, ray);
}

ray3f sampleCamera(const Camera* camera, const vec2f& is, const vec2f& ls) {
    ray3f ray;
    switch(camera->type) {
        case PerspectiveCamera::TYPEID: {
            PerspectiveCamera* cam = (PerspectiveCamera*)camera;
            vec3f lp = x3f*((ls.x-0.5f)*cam->aperture)+
                       y3f*((ls.y-0.5f)*cam->aperture);
            vec3f ip = x3f*((is.x-0.5f)*cam->width)+
                       y3f*((is.y-0.5f)*cam->height)+
                       z3f*(cam->distance);
            ray = ray3f(lp,normalize(ip-lp));
        } break;
        case OrthogonalCamera::TYPEID: {
            OrthogonalCamera *cam = (OrthogonalCamera*)camera;
            vec3f ip = x3f * (is.x - 0.5f) * cam->width +
                       y3f * (is.y - 0.5f) * cam->height;
            ray = ray3f(ip, z3f);
        } break;
        case PanoramicCamera::TYPEID: {
            float phi = is.x * consts<float>::twopi;
            float theta = (1-is.y) * consts<float>::pi;
            float s = sin(theta); float c = cos(theta);
            vec3f d = vec3f(s * cos(phi), c, s * sin(phi));
            ray = ray3f(zero3f, d);
        } break;
        case FisheyeCamera::TYPEID: {
            vec2f v = (is - half2f) * 2.0f;
            float r2 = v.lengthSqr();
            float r = sqrt(r2);
            float sinp = v.x / r;
            float cosp = v.y / r;
            float sint = min(1.0f, r);
            float cost = max(0.0f, sqrtf(1 - r2));
            vec3f d = vec3f(sinp*sint, cosp*sint, cost);
            if (r2 >= 1.0f) ray = ray3f(zero3f, d, ray3f::infinity, ray3f::epsilon);
            else ray = ray3f(zero3f, normalize(d));
        } break;
        default: error("Unknown camera type."); return ray3f(zero3f,z3f);
    }
    return transformRay(camera->transform, ray);
}

ShapeSample sampleShape(const Shape* shape, const vec2f& s) {
    ShapeSample ss;
    switch(shape->type) {
        case QuadShape::TYPEID: {
            QuadShape* quad = (QuadShape*)shape;
            ss.frame = frame3f(
                vec3f((s.x - 0.5f)*quad->width,(s.y - 0.5f)*quad->height,0),
                x3f, y3f, z3f);
            ss.area = quad->width*quad->height;
        } break;
        default: error("unsupported shape to sample"); break;
    }
    return ss;
}

float shapeArea(const Shape* shape) {
    float a = 0;
    switch(shape->type) {
        case QuadShape::TYPEID: {
            QuadShape* quad = (QuadShape*)shape;
            a = quad->width * quad->height;
        } break;
        case TriangleMeshShape::TYPEID: {
            TriangleMeshShape* mesh = (TriangleMeshShape*)shape;
            for (int i = 0; i < mesh->face.size(); i++) {
                vec3i f = mesh->face[i];
                vec3f v0 = mesh->pos[f.y] - mesh->pos[f.x];
                vec3f v1 = mesh->pos[f.z] - mesh->pos[f.x];
                a += 0.5f * cross(v0, v1).lengthSqr();
            }
        }
        default: error("unsupported shape to sample"); break;
    }
    return a;
}

LightSample sampleLight(const std::vector<Light*>& lights, float s) {
    int l = sampleUniform(s, lights.size());
    LightSample ls;
    ls.light = lights[l];
    ls.pdf = 1.0f / lights.size();
    return ls;
}

ShadowSample sampleShadow(const Light* light, const vec3f& p, const vec2f& s) {
    ShadowSample ss;
    vec3f pl = transformPointInverse(light->transform, p);
    switch(light->type) {
        case PointLight::TYPEID: {
            PointLight* point = (PointLight*)light;
            ss.le = point->intensity / pl.lengthSqr();
            ss.ray = makeraysegment3f(pl,zero3f);
            ss.pdf = 1;
        } break;
        case DirectionalLight::TYPEID: {
            DirectionalLight* direct = (DirectionalLight*)light;
            ss.le = direct->radiance;
            ss.ray = ray3f(pl,-z3f);
            ss.pdf = 1;
        } break;
        case AreaLight::TYPEID: {
            AreaLight* area = (AreaLight*)light;
            ShapeSample ssl = sampleShape(area->shape,s);
            EmissionSample es = sampleEmission(area->material, s);
            ss.ray = makeraysegment3f(pl,ssl.frame.o);
            float c = max(dot(-ss.ray.d, ssl.frame.z),0.0f); // cosine of the angle
            if(c > 0) {
                ss.le = evalEmission(es,ssl.frame,-ss.ray.d);
                ss.pdf = (pl-ssl.frame.o).lengthSqr() / (c * ssl.area);
            } else {
                ss.le = zero3f;
                ss.pdf = 0;
            }
        } break;
        case EnvironmentLight::TYPEID: {
            EnvironmentLight* env = (EnvironmentLight*)light;
            if(options::scene::envmapsampling && env->leTxt && env->traceAux) {
                EnvironmentLightAux* aux = (EnvironmentLightAux*)env->traceAux;
                float mapPdf;
                vec2f uv = aux->dist.sample(s, &mapPdf);
                ss.ray = ray3f(pl, latlongToDirection(uv));
                // Compute PDF for sampled infinite light direction
                float sinTheta = sin(uv.y * consts<float>::pi);
                ss.pdf = mapPdf / (consts<float>::twopi * consts<float>::pi * sinTheta);
                if (sinTheta == 0.f) { ss.pdf = 0.f; ss.le = zero3f; }
                else {
                    if(env->leTxt) ss.le = env->le * sampleImage(env->leTxt->image, uv);
                    else ss.le = env->le;
                }
            } else {
                vec3f w = sampleSpherical(s);
                ss.ray = ray3f(pl, w);
                ss.pdf = sampleSphericalPdf(w);
                vec2f uv = directionToLatlong(w);
                if(env->leTxt) ss.le = env->le * sampleImage(env->leTxt->image, uv);
                else ss.le = env->le;
            }
        } break;
        default: error("unsupported light type");
    }
    ss.ray = transformRay(light->transform, ss.ray);
    return ss;
}

ShadowSample sampleShadow(const Light* light, const vec3f& p, const vec3f& wi) {
    switch(light->type) {
        case PointLight::TYPEID: return invalidShadowSample;
        case DirectionalLight::TYPEID: return invalidShadowSample;
        case AreaLight::TYPEID: {
            AreaLight* area = (AreaLight*)light;
            AcceleratorSurface surface(area->shape, area->transform, area->material, area);
            intersection3f intersection;
            if(!AcceleratorCommon::intersectFirst(&surface, ray3f(p, wi), intersection))
                return invalidShadowSample;
            const frame3f& frame = intersection.f; // frame in world coordinate
            vec3f wo = -wi;
            float proj = max(0.0f, dot(frame.z, wi));
            EmissionSample es = sampleEmission(area->material, intersection.st);

            if (proj > 0) {
                ShadowSample ss;
                ss.le = evalEmission(es, frame, wo);
                ss.pdf = (frame.o - p).lengthSqr() / (shapeArea(area->shape) * proj);
                ss.ray = makeraysegment3f(p, frame.o);
                return ss;
            } else
                return invalidShadowSample;
        }
        case EnvironmentLight::TYPEID: {
            EnvironmentLight* env = (EnvironmentLight*)light;
            ShadowSample ss;
            vec3f d = normalize(transformVectorInverse(light->transform, wi));
            vec2f uv = directionToLatlong(d);
            if(options::scene::envmapsampling && env->leTxt && env->traceAux) {
                EnvironmentLightAux* aux = (EnvironmentLightAux*)env->traceAux;
                float sinTheta = sin(uv.y * consts<float>::pi);
                if (sinTheta <= 0.f) {
                    ss.pdf = 0.f;
                    ss.le = zero3f;
                } else {
                    ss.pdf = aux->dist.pdf(uv) / (consts<float>::twopi * consts<float>::pi * sinTheta);
                    if(env->leTxt) ss.le = env->le * sampleImage(env->leTxt->image, uv);
                    else ss.le = env->le;
                }
            } else {
                if(env->leTxt) ss.le = env->le * sampleImage(env->leTxt->image, uv);
                else ss.le = env->le;
                ss.pdf = sampleSphericalPdf(d);
            }
            ss.ray = ray3f(p, wi);
            return ss;
        } break;
        default: error("unsupported light type");
    }
    return invalidShadowSample;
}


float sampleShadowPdf(const Light* light, const vec3f& p, const vec3f& wi) {
    switch(light->type) {
        case PointLight::TYPEID: return 0;
        case DirectionalLight::TYPEID: return 0;
        case AreaLight::TYPEID: {
            AreaLight* area = (AreaLight*)light;
            // intersect the shape to see if this is a good hit
            AcceleratorSurface surface(area->shape, area->transform, area->material, area);
            intersection3f intersection;
            if(!AcceleratorCommon::intersectFirst(&surface, ray3f(p,wi), intersection)) return 0;
            const frame3f& frame = intersection.f; //frame in world coordinate
            vec3f wo = -wi;
            float proj = dot(frame.z, wo);
            return (proj < 0) ? 0 : (frame.o - p).lengthSqr() / (shapeArea(area->shape) * proj);
        }
        case EnvironmentLight::TYPEID: {
            EnvironmentLight* env = (EnvironmentLight*)light;
            if(options::scene::envmapsampling && env->leTxt && env->traceAux) {
                EnvironmentLightAux* aux = (EnvironmentLightAux*)env->traceAux;
                vec3f d = normalize(transformVectorInverse(light->transform, wi));
                vec2f uv = directionToLatlong(d);
                float sinTheta = sin(uv.y * consts<float>::pi);
                if (sinTheta <= 0.f) return 0.f;
                return aux->dist.pdf(uv) / (consts<float>::twopi * consts<float>::pi * sinTheta);
            } else {
                return sampleSphericalPdf(wi);
            }
        }
        default: error("unsupported source type"); return 0;
    }
}

PhotonSample samplePhoton( const Light* light, const vec2f &ss, const vec2f &sa, const vec3f &sceneCenter, float sceneRadius) {
    PhotonSample ps;
    switch(light->type) {
        case PointLight::TYPEID: {
            PointLight* l = (PointLight*)light;
            ps.le = l->intensity;
            vec3f direction = sampleSpherical(sa);
            ps.pdf = sampleSphericalPdf(direction);
            ps.ray = ray3f(zero3f, direction);
            ps.normal = direction;
        } break;
        case DirectionalLight::TYPEID: {
            DirectionalLight* l = (DirectionalLight*)light;
            ps.le = l->radiance;
            vec2f duv = sampleConcentricDisk(sa);
            vec3f center = transformPointInverse(l->transform, sceneCenter);
            vec3f diskp = center + (x3f * duv.x + y3f * duv.y) * sceneRadius;
            vec3f origin = diskp - z3f * (sceneRadius);
            ps.ray = ray3f(origin, z3f);
            ps.normal = z3f;
            ps.pdf = 1.0f / (consts<float>::pi * sceneRadius * sceneRadius);
        } break;
        case AreaLight::TYPEID: {
            AreaLight* l = (AreaLight*)light;
            ShapeSample ssl = sampleShape(l->shape, ss);
            EmissionSample es = sampleEmission(l->material, ss);
            vec3f direction = sampleHemisphericalCos(sa);
            float pdf = sampleHemisphericalCosPdf(direction);
            ps.normal = ssl.frame.z;
            ps.ray = ray3f(ssl.frame.o, direction);
            if(dot(ps.ray.d, ssl.frame.z) > 0) {
                ps.le = evalEmission(es,ssl.frame,ps.ray.d);
                ps.pdf = (1.0f / ssl.area) * pdf;
            } else {
                ps.le = zero3f;
                ps.pdf = 0;
            }
        } break;
        case EnvironmentLight::TYPEID: {
            EnvironmentLight *l = (EnvironmentLight*)light;
            vec2f uv;
            float directionPdf = 0;
            if(options::scene::envmapsampling && l->leTxt && l->traceAux) {
                EnvironmentLightAux* aux = (EnvironmentLightAux*)l->traceAux;
                float mapPdf;
                uv = aux->dist.sample(ss, &mapPdf);
                if (mapPdf == 0) { ps.le = zero3f; ps.pdf = 0; break; }
                ps.normal = -latlongToDirection(uv);
                float sintheta = sinf(uv.y);
                if (sintheta == 0.0f) directionPdf = 0;
                else directionPdf = mapPdf / (consts<float>::twopi * consts<float>::pi * sintheta);
            } else {
                vec3f d = sampleSpherical(ss);
                ps.normal = -d;
                directionPdf = sampleSphericalPdf(d);
                uv = directionToLatlong(d);
            }
            vec3f center = transformPointInverse(l->transform, sceneCenter);
            vec3f x, y;
            xyFromZ(ps.normal, x, y);
            vec2f duv = sampleConcentricDisk(sa);
            vec3f diskp = center + (duv.x * x + duv.y * y) * sceneRadius;
            ps.ray = ray3f(diskp - sceneRadius * ps.normal, ps.normal);
            float areaPdf = 1.f / (consts<float>::pi * sceneRadius * sceneRadius);
            ps.pdf = directionPdf * areaPdf;
            if(l->leTxt) ps.le = l->le * sampleImage(l->leTxt->image, uv);
            else ps.le = l->le;
        } break;
        default: error("unsupported source type");
    }
    ps.ray = transformRay(light->transform, ps.ray);
    ps.normal = transformNormal(light->transform, ps.normal);
    return ps;
}

bool isDelta(const Light* light) {
    switch(light->type) {
        case PointLight::TYPEID: return true;
        case DirectionalLight::TYPEID: return true;
        case AreaLight::TYPEID: return false;
        case EnvironmentLight::TYPEID: return false;
        default: error("unknown source type"); return false;
    }
}

// differece between sample image and texture
// is that sample image will not wrap and flip y
// no boundary check
vec3f sampleImage(const Image<vec3f> &image, const vec2f &uv) {
    int i = static_cast<int>(uv.x * image.width());
	int j = static_cast<int>(uv.y * image.height());
    return image.at(i, j);
}

vec3f sampleTexture(const Texture* txt, const vec2f& uv) {
    if (txt->image.size() == 0) {
        return one3f;
    }
    int i = static_cast<int>(uv.x * txt->image.width());
    int j = static_cast<int>(uv.y * txt->image.height());
    int w = txt->image.width();
    int h = txt->image.height();
    switch(txt->wrap) {
    case Texture::REPEAT:
        i = i % w;
        j = j % h;
        if(i < 0) i += w;
        if(j < 0) j += h;
        break;
    case Texture::CLAMP:
        i = clamp(i, 0, w - 1);
        j = clamp(j, 0, h - 1);
        break;
    case Texture::BLACK:
        if (i < 0 || i >= w || j < 0 || j >= h)
            return zero3f;
        break;
    }
    return txt->image.at(i,txt->image.height()-j-1);
}

vec3f sampleSpatialVariation(const vec3f& v,
                                     Texture* txt, const vec2f& uv) {
    if(txt) return v * sampleTexture(txt,uv);
    else return v;
}

float sampleSpatialVariation(float v, Texture* txt, const vec2f& uv) {
    if(txt) return v * mean(sampleTexture(txt,uv));
    else return v;
}

EmissionSample sampleEmission(const Material* m, const vec2f& uv) {
    if (!m) return invalidEmissionSample;
    EmissionSample es;
    switch(m->type) {
        case LambertEmissionMaterial::TYPEID: {
            es.type = LambertEmissionMaterial::TYPEID;
            LambertEmissionMaterial* emission = (LambertEmissionMaterial*)m;
            es.le = sampleSpatialVariation(emission->le, emission->leTxt, uv);
        } break;
        case LambertMaterial::TYPEID:
        case MicrofacetMaterial::TYPEID:
        case KajiyaHairMaterial::TYPEID: return invalidEmissionSample;
        default: error("unknown emission type");
    }
    return es;
}

ReflectanceSample sampleReflectance(const Material* m, const vec2f& uv) {
    if (!m) return invalidReflectanceSample;
    ReflectanceSample rs = invalidReflectanceSample;
    switch(m->type) {
        case LambertMaterial::TYPEID: {
            rs.type = LambertMaterial::TYPEID;
            const LambertMaterial* reflectance = (const LambertMaterial*)m;
            rs.rhod = sampleSpatialVariation(reflectance->rhod,reflectance->rhodTxt,uv);
        } break;
        case MicrofacetMaterial::TYPEID: {
            rs.type = MicrofacetMaterial::TYPEID;
            const MicrofacetMaterial* reflectance = (const MicrofacetMaterial*)m;
            rs.rhod = sampleSpatialVariation(reflectance->rhod,reflectance->rhodTxt,uv);
            rs.rhos = sampleSpatialVariation(reflectance->rhos,reflectance->rhosTxt,uv);
            rs.n = sampleSpatialVariation(reflectance->n,reflectance->nTxt,uv);
            rs.diffuseMode = reflectance->diffuseMode;
            rs.specularMode = reflectance->specularMode;
        } break;
        case KajiyaHairMaterial::TYPEID: {
            rs.type = KajiyaHairMaterial::TYPEID;
            const KajiyaHairMaterial* reflectance = (const KajiyaHairMaterial*)m;
            rs.rhod = sampleSpatialVariation(reflectance->rhod,reflectance->rhodTxt,uv);
        } break;
        case LambertEmissionMaterial::TYPEID: return rs;
        default: {
            error_va("[sampleReflectance] unknown reflectance type %d", m->type);
        }
    }
    return rs;
}

vec3f sampleEnvironment(const Light* light, const vec3f& wo) {
    switch(light->type) {
        case PointLight::TYPEID:
        case DirectionalLight::TYPEID:
        case AreaLight::TYPEID:
            return zero3f;
        case EnvironmentLight::TYPEID: {
            EnvironmentLight* env = (EnvironmentLight*)light;
            if(env->leTxt) {
                vec3f wol = transformVectorInverse(light->transform, wo);
                vec2f uv = directionToLatlong(wol);
                return env->le * sampleImage(env->leTxt->image,uv);
            } else return env->le;
        } break;
        default:
            error("unknown source type"); return zero3f;
    }
}

ScatteringSample sampleScattering(const ReflectanceSample& rs,
                                  const frame3f& f, const vec3f& wo,
                                  float lr, const vec2f& ar) {
    if (!isvalid(rs)) return invalidScatteringSample;
    vec3f wol = f.transformVectorInverse(wo);
    vec3f wil; vec3f bsdf; float pdf = 0;
    if(!options::scene::brdfsampling) {
        if(wol.z <= 0) return invalidScatteringSample;
        wil = sampleHemisphericalCos(ar);
        bsdf = evalReflectance(rs, wil, wol);
        pdf = sampleHemisphericalCosPdf(wil);
    } else {
        switch(rs.type) {
            case LambertMaterial::TYPEID: {
                if(wol.z <= 0) return invalidScatteringSample;
                wil = sampleHemisphericalCos(ar);
                bsdf = evalLambertReflectance(rs.rhod, wil, wol);
                pdf = sampleHemisphericalCosPdf(wil);
            } break;
            case MicrofacetMaterial::TYPEID: {
                if(wol.z <= 0) return invalidScatteringSample;
                float diffP = mean(rs.rhod) / (mean(rs.rhod)+mean(rs.rhos));
                float specP = 1 - diffP;
                vec3f whl;
                if(lr < diffP) {
                    wil = sampleHemisphericalCos(ar);
                    whl = normalize(wol+wil);
                } else {
                    // from pbrt
                    whl = sampleHemisphericalCosPower(ar, rs.n);
                    wil = whl * (2 * dot(wol,whl)) - wol;
                }
                if(wil.z <= 0) return invalidScatteringSample;
                float oDh = dot(whl, wol);
                if(oDh <= 0) return invalidScatteringSample;
                bsdf = evalMicrofacetReflectance(rs.rhod, rs.rhos, rs.n,
                    rs.diffuseMode, rs.specularMode, wil, wol);
                float diffPdf = sampleHemisphericalCosPdf(wil);
                float specPdf = sampleHemisphericalCosPowerPdf(whl, rs.n) /
                    (4 * oDh);
                pdf = diffP*diffPdf + specP*specPdf;
            } break;
            case KajiyaHairMaterial::TYPEID: {
                wil = sampleSpherical(ar);
                bsdf = evalKajiyaHairReflectance(rs.rhod, wil, wol);
                pdf = sampleSphericalPdf(wil);
            } break;
            default: { error_va("unknown reflectance type %d", rs.type); return invalidScatteringSample; }
        }
    }
    ScatteringSample ss = { pdf <= 0 ? zero3f : bsdf, ray3f(f.o,f.transformVector(wil)), pdf };
    return ss;
}

float sampleScatteringPdf(const ReflectanceSample& rs, const frame3f& f,
                          const vec3f& wo, const vec3f& wi) {
    vec3f wol = f.transformVectorInverse(wo);
    vec3f wil = f.transformVectorInverse(wi);
    if(!options::scene::brdfsampling) {
        if(wol.z <= 0 || wil.z <= 0) return 0;
        return sampleHemisphericalCosPdf(wil);
    } else {
        switch(rs.type) {
            case LambertMaterial::TYPEID: {
                if(wol.z <= 0 || wil.z <= 0) return 0;
                return sampleHemisphericalCosPdf(wil);
            } break;
            case MicrofacetMaterial::TYPEID: {
                if(wol.z <= 0 || wil.z <= 0) return 0;
                float diffP = mean(rs.rhod) / (mean(rs.rhod)+mean(rs.rhos));
                float specP = 1 - diffP;
                vec3f whl = normalize(wil+wol);
                float oDh = dot(whl, wol);
                if(oDh <= 0) return 0;
                float diffPdf = sampleHemisphericalCosPdf(wil);
                float specPdf = sampleHemisphericalCosPowerPdf(whl, rs.n) /
                (4 * oDh);
                return diffP*diffPdf + specP*specPdf;
            } break;
            case KajiyaHairMaterial::TYPEID: {
                return sampleHemisphericalPdf(wil);
            } break;
            default: error_va("unknown reflectance type %d", rs.type); return 0;
        }
    }
}

vec3f evalEmission(const EmissionSample& es,
                   const frame3f& f, const vec3f& wo) {
    switch(es.type) {
        case LambertEmissionMaterial::TYPEID:
            return evalLambertEmission(es.le, f, wo);
        case LambertMaterial::TYPEID:
		case MicrofacetMaterial::TYPEID:
        case KajiyaHairMaterial::TYPEID: return zero3f;
        default: error("Unknown emission"); return zero3f;
    }
    return zero3f;
}

vec3f evalReflectance(const ReflectanceSample& rs, const frame3f& f,
					  const vec3f& wi, const vec3f& wo) {
	switch(rs.type) {
		case LambertMaterial::TYPEID:
            return evalLambertReflectance(rs.rhod, f, wi, wo);
		case MicrofacetMaterial::TYPEID:
            return evalMicrofacetReflectance(rs.rhod, rs.rhos, rs.n,
                rs.diffuseMode, rs.specularMode, f, wi, wo);
        case KajiyaHairMaterial::TYPEID:
            return evalKajiyaHairReflectance(rs.rhod, f, wi, wo);
        case LambertEmissionMaterial::TYPEID: return zero3f;
		default: error("Unknown reflectance"); return zero3f;
	}
}

vec3f evalReflectance(const ReflectanceSample& rs, const vec3f& wil, const vec3f& wol) {
	switch(rs.type) {
		case LambertMaterial::TYPEID:
            return evalLambertReflectance(rs.rhod, wil, wol);
		case MicrofacetMaterial::TYPEID:
            return evalMicrofacetReflectance(rs.rhod, rs.rhos, rs.n,
                                             rs.diffuseMode, rs.specularMode,
                                             wil, wol);
        case KajiyaHairMaterial::TYPEID:
            return evalKajiyaHairReflectance(rs.rhod, wil, wol);
        case LambertEmissionMaterial::TYPEID: return zero3f;
		default: error("Unknown reflectance"); return zero3f;
	}
}

ReflectanceSample reflectanceToLambert(const ReflectanceSample& rs) {
    ReflectanceSample ret;
	switch(rs.type) {
		case LambertMaterial::TYPEID:
		case MicrofacetMaterial::TYPEID:
            ret.rhod = rs.rhod;
            ret.type = LambertMaterial::TYPEID;
            break;
        case KajiyaHairMaterial::TYPEID: ret.rhod = rs.rhod;
            ret.type = KajiyaHairMaterial::TYPEID;
            break;
		default:
            error("[reflectanceToLambert] unknown reflectance");
            ret = invalidReflectanceSample;
    }
    return ret;
}

void initLightSampling(const std::vector<Light*>& lights) {
    for(int i = 0; i < lights.size(); i ++) {
        Light* light = lights[i];
        switch(light->type) {
            case PointLight::TYPEID: break;
            case DirectionalLight::TYPEID: break;
            case AreaLight::TYPEID: break;
            case EnvironmentLight::TYPEID: {
                EnvironmentLight* env = (EnvironmentLight*)light;
                if(!env->traceAux && options::scene::envmapsampling && env->leTxt) {
                    EnvironmentLightAux* aux = new EnvironmentLightAux();
                    const Image<vec3f>& txt = env->leTxt->image;
                    Image<float> func(txt.width(),txt.height());
                    for (int v = 0; v < txt.height(); ++v) {
                        float sinTheta = sin(consts<float>::pi * float(v+.5f)/float(txt.height()));
                        for (int u = 0; u < txt.width(); ++u) {
                            func.at(u,v) = mean(txt.at(u, v));
                            func.at(u,v) *= sinTheta;
                        }
                    }
                    aux->dist.init(func);
                    env->traceAux = aux;
                }
            } break;
            default: error("unknown source type");
        }
    }
}
