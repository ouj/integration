#include "integration.h"
#include <accelerator/accelerator.h>
#include "options.h"
#include <scene/light.h>

void initImageSamplingState(ImageSamplingState& state, const ImageSamplingOpts& opts,
                            int width, int height) {
    // initialize image
    state.image.assign(width, height, zero3f);
    state.accimage.assign(width, height, zero3f);
    state.accweight.assign(width, height,0.0f);
    state.samples.assign(width, height,0);
    // initialize random number generators
    error_if_va(opts.generatortype == ParametricFastSobolSampleGenerator::TYPEID &&
                opts.maxsamples > options::trace::maxsamples,
                "maxsamples cannot be larger than %d", options::trace::maxsamples);
    state.generator = initGenerator(opts.generatortype, width, height, opts.batchsamples);
    // rngs
    state.rngs = initRngs(width, height);
}

vec3f evalEnvironment(const ray3f& ray, const std::vector<Light*>& lights) {
    vec3f le = zero3f;
    for(int l = 0; l < lights.size(); l ++) {
        le += sampleEnvironment(lights[l], ray.d);
    }
    return le;
}

vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f,
                 const vec3f& wo, const vec2f& ls,
                 const Light* light, const Accelerator* accelerator) {
    // sample shadow
    ShadowSample ss = sampleShadow(light, f.o, ls);
    if(ss.le.iszero()) return zero3f;

    // accumulate direct
    vec3f brdf = evalReflectance(rs,f,ss.ray.d,wo);
    float projection = dot(ss.ray.d, f.z);
    vec3f ld = ss.le * brdf * projection / ss.pdf;

    if(ld.iszero()) return zero3f;
    if(accelerator->intersectAny(ss.ray)) return zero3f;
    else return ld;
}

vec3f evalDirectMis(const ReflectanceSample& rs,
                    const frame3f& f, const vec3f& wo,
                    const vec2f& ls, float bls, const vec2f& bas,
                    const Light* light, const Accelerator* accelerator) {
    error_if_not(!isDelta(light), "MIS only works for non-delta lights");
    // init values
    vec3f ld = zero3f;

    // sample according to the light
    ShadowSample lss = sampleShadow(light, f.o, ls);
    if(!lss.le.iszero()) {
        vec3f ldl = lss.le * evalReflectance(rs,f,lss.ray.d,wo) *
        dot(lss.ray.d, f.z) / lss.pdf;
        if(!ldl.iszero()) {
            if(!accelerator->intersectAny(lss.ray)) {
                float brdfPdf = sampleScatteringPdf(rs, f, wo, lss.ray.d);
                float w = powerHeuristic(lss.pdf, brdfPdf);
                ld += ldl * w;
            }
        }
    }

    // sample according to the brdf
    ScatteringSample bss = sampleScattering(rs, f, wo, bls, bas);
    if(!bss.bsdf.iszero()) {
        ShadowSample ss = sampleShadow(light, f.o, bss.ray.d);
        if (ss.pdf > 0) {
            float w = powerHeuristic(bss.pdf, ss.pdf);
            vec3f ldb = zero3f;
            if (!accelerator->intersectAny(ss.ray)) {
                ldb = ss.le;
            }
            if(!ldb.iszero()) {
                ld += ldb * bss.bsdf * dot(bss.ray.d, f.z) / bss.pdf * w;
            }
        }
    }

    return ld;
}

vec3f evalDirectBrdf(const ReflectanceSample& rs, const frame3f& f,
                     const vec3f& wo, float bls, const vec2f& bas,
                     const Light* light, const Accelerator* accelerator) {
    error_if_not(!isDelta(light), "brdf sampling only works for non-delta lights");
    // init values
    vec3f ld = zero3f;

    // sample according to the brdf
    ScatteringSample bss = sampleScattering(rs, f, wo, bls, bas);
    if(!bss.bsdf.iszero()) {
        ShadowSample ss = sampleShadow(light, f.o, bss.ray.d);
        vec3f ldb = zero3f;
        if (ss.pdf > 0) {
            if (!accelerator->intersectAny(ss.ray)) {
                ldb = ss.le;
            }
        }
        if(!ldb.iszero()) {
            float cost = dot(bss.ray.d, f.z);
            ld += ldb * bss.bsdf * cost / bss.pdf;
        }
    }
    return ld;
}


vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                 SampleSequence& seq, TraceDirectMode mode,
                 const std::vector<Light*>& lights, const Accelerator* accelerator) {
    vec3f ld = zero3f;
    switch(mode) {
        case TDM_ALL: {
            for(int l = 0; l < lights.size(); l ++) {
                ld += evalDirect(rs, f, wo, next2f(seq), lights[l], accelerator);
            }
        } break;
        case TDM_ALL_MIS: {
            for(int l = 0; l < lights.size(); l ++) {
                if(isDelta(lights[l]))
                    ld += evalDirect(rs, f, wo, next2f(seq), lights[l], accelerator);
                else
                    ld += evalDirectMis(rs, f, wo, next2f(seq), next1f(seq),
                                        next2f(seq), lights[l], accelerator);
            }
        } break;
        case TDM_ALL_BRDF: {
            for(int l = 0; l < lights.size(); l ++) {
                if(isDelta(lights[l]))
                    ld += evalDirect(rs, f, wo, next2f(seq), lights[l], accelerator);
                else
                    ld += evalDirectBrdf(rs, f, wo, next1f(seq), next2f(seq),
                                         lights[l], accelerator);
            }
        } break;
        case TDM_UNIFORM: {
            LightSample ls = sampleLight(lights,next1f(seq));
            ld = evalDirect(rs, f, wo, next2f(seq), ls.light, accelerator) / ls.pdf;
        } break;
        case TDM_UNIFORM_MIS: {
            LightSample ls = sampleLight(lights,next1f(seq));
            if(isDelta(ls.light)) ld = evalDirect(rs, f, wo, next2f(seq), ls.light, accelerator) / ls.pdf;
            else ld = evalDirectMis(rs, f, wo, next2f(seq), next1f(seq), next2f(seq), ls.light, accelerator) / ls.pdf;
        } break;
        case TDM_UNIFORM_BRDF: {
            LightSample ls = sampleLight(lights,next1f(seq));
            if(isDelta(ls.light)) ld = evalDirect(rs, f, wo, next2f(seq), ls.light, accelerator) / ls.pdf;
            else ld = evalDirectBrdf(rs, f, wo, next1f(seq), next2f(seq), ls.light, accelerator) / ls.pdf;;
        }
        default: error("unknown trace direct mode");
    }
    return ld;
}

bool traceRay(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
              const std::vector<Light*> &lights, const Accelerator *accelerator,
              frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs) {
    // init
    le = zero3f;
    rs = invalidReflectanceSample;

    // intersect
    intersection3f intersection;
    if(!accelerator->intersectFirst(ray, intersection)) {
        if(resolveEnvironment) le = evalEnvironment(ray, lights);
        return false;
    }

    // shading frame
    f = intersection.f;
    wo = -ray.d;

    // emission
    if(resolveEmission) {
        EmissionSample es = sampleEmission(intersection.m,intersection.st);
        if(isvalid(es)) le = evalEmission(es,f,-ray.d);
    }

    // reflectance
    rs = sampleReflectance(intersection.m,intersection.st);

    // done
    return true;
}
