#ifndef _TRACE_UTILS_H_
#define _TRACE_UTILS_H_
#include <scene/camera.h>
#include <scene/scene.h>
#include <scene/transform.h>

inline int adjustedHeight(const Camera *camera, int width) {
    float ratio = 1.0f;
    switch (camera->type) {
        case PerspectiveCamera::TYPEID: {
            PerspectiveCamera* cam = (PerspectiveCamera*)camera;
            ratio = cam->height / cam->width;
        } break;
        case OrthogonalCamera::TYPEID: {
            OrthogonalCamera *cam = (OrthogonalCamera*)camera;
            ratio = cam->height / cam->width;
        } break;
        case PanoramicCamera::TYPEID: {
            ratio = 0.5f;
        } break;
        case FisheyeCamera::TYPEID: {
            ratio = 1.0f;
        } break;
        default: error("unknown lens type in adjustImageSize()");
    }
    return (int)(ratio * width);
}

vec2i projectToCamera(const vec3f &wp, const Camera* camera, int width, int height);
std::vector<range2i> makeImageBlocks(int width, int height, int blocksize);

#endif