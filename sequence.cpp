#include "sequence.h"
#include <string>
#include <common/func.h>
#include <common/array.h>


SampleGenerator* initGenerator(int type, int w, int h, int s) {
    switch (type) {
        case CenterSampleGenerator::TYPEID: return new CenterSampleGenerator();
        case RandomSampleGenerator::TYPEID: return new RandomSampleGenerator();
        case PixelHaltonSampleGenerator::TYPEID: return new PixelHaltonSampleGenerator();
        case PixelSobolSampleGenerator::TYPEID: return new PixelSobolSampleGenerator();
#ifdef USE_STRATIFIED_SAMPLES
        case StratifiedSampleGenerator::TYPEID: return new StratifiedSampleGenerator();
#endif
        case ParametricFastSobolSampleGenerator::TYPEID: {
            ParametricFastSobolSampleGenerator* generator = new ParametricFastSobolSampleGenerator();
            initParametricFastSobolSampleGenerator(generator, w, h, s);
            return generator;
        } break;
        default: { error("unknown generator type"); return 0; }
    }
}
void initParametricFastSobolSampleGenerator(ParametricFastSobolSampleGenerator* generator,
                                  int w, int h, int maxs) {
    // determine size
    generator->r = 1; generator->m = 0;
    while(generator->r < w || generator->r < h) {
        generator->r *= 2;
        generator->m ++;
    }

    // resize arrays
    int ns = ParametricFastSobolSampleGenerator::maxs;
    int r = generator->r;
    int m = generator->m;
    generator->c0Lower.resize(ns, r);
    generator->c0Upper.resize(ns, r);
    generator->c1Lower.resize(ns, r);
    generator->c1Upper.resize(ns, r);
    generator->c0LowerInverse.resize(ns, r);
    generator->c1UpperInverse.resize(ns, r);

    // prepapre lookup tables
    // from keller's patent
    for(int s = 0; s < ns; s ++) {
        unsigned int shift = s << (m << 1);

        for(unsigned int i = 0; i < (1u << m); i ++) {
            unsigned int j = qmcVanDerCorput32u(i);
            generator->c0Lower.at(s,i) = j;
            generator->c0LowerInverse.at(s,j >> (32 - m)) = i;
            generator->c0Upper.at(s,i) = qmcVanDerCorput32u((i << m) | shift);

            generator->c1Lower.at(s,i) = qmcSobol232u(i);
            unsigned int k = qmcSobol232u((i << m) | shift);
            generator->c1Upper.at(s,i) = k;
            generator->c1UpperInverse.at(s,k >> (32 - m)) = i;
        }
    }
}

inline unsigned int lookupFastSobolSampleGenerator(ParametricFastSobolSampleGenerator* generator,
    unsigned int s, unsigned int i, unsigned int j) {
    unsigned int lower = generator->c0LowerInverse.at(s,i);
    unsigned int delta = j ^ (generator->c1Lower.at(s,lower) >>
                              (32 - generator->m));
    unsigned int upper = generator->c1UpperInverse.at(s,delta);
    unsigned int index = ((upper << generator->m) | lower) |
                          (s << (generator->m << 1));
    //x = generator->c0Lower[lower] ^ generator->c0Upper[upper];
    //y = generator->c1Lower[lower] ^ generator->c1Upper[upper];
    return index;
}

// i width
// j height
// s number of samples for this pixel
// ns number of samples for this batch
void initSequence(SampleSequence& seq, const SampleGenerator* generator,
                  Rng& rng, int i, int j, int s, int ns) {
    seq.generator = generator;
    seq.rng = &rng;
    seq.i = i;  // pixel coordinate
    seq.j = j;  // pixel coordinate
    seq.s = s;  // current sample number for this pixel
    seq.ss = s; // current sample number in this batch
    seq.ns = ns;// number of samples for this batch
    seq.d = 0;  // current dimension

#ifdef USE_STRATIFIED_SAMPLES
    seq.d1 = 0;
    seq.d2 = 0;
#endif

    switch (seq.generator->type) {
        case CenterSampleGenerator::TYPEID: break;
        case RandomSampleGenerator::TYPEID: break;
        case PixelHaltonSampleGenerator::TYPEID: break;
        case PixelSobolSampleGenerator::TYPEID: break;
#ifdef USE_STRATIFIED_SAMPLES
        case StratifiedSampleGenerator::TYPEID: {
            // check pow2
            int ns2 = (int)sqrt((float)ns);
            error_if_not(ns2*ns2 == ns, "number of samples is not a power of 2");
            warning_if_not(ns <= SampleSequence::maxs, "too large number of samples");
            for(int i = 0; i < SampleSequence::max2d; i ++) {
                stratify2d(seq.samples2[i].data(), ns2, ns2, *seq.rng);
                if(i != 0) scramble(seq.samples2[i].data(), ns, *seq.rng);
            }
            for(int i = 0; i < SampleSequence::max1d; i ++) {
                stratify1d(seq.samples1[i].data(), ns, *seq.rng);
                scramble(seq.samples1[i].data(), ns, *seq.rng);
            }
        } break;
#endif
        case ParametricFastSobolSampleGenerator::TYPEID: break;
        default: error("sequence type unknown");
    }
}

void nextSample(SampleSequence& seq) {
    seq.s += 1;
    seq.d = 0;

#ifdef USE_STRATIFIED_SAMPLES
    seq.d1 = 0;
    seq.d2 = 0;
#endif
}

float next1f(SampleSequence& seq) {
    float ret = 0.0f;
    switch (seq.generator->type) {
        case CenterSampleGenerator::TYPEID: ret = 0.5f; break;
        case RandomSampleGenerator::TYPEID: ret = seq.rng->next1f(); break;
        case PixelHaltonSampleGenerator::TYPEID: ret = (float)qmcHalton(seq.s, seq.d); break;
        case PixelSobolSampleGenerator::TYPEID: ret = (float)qmcSobol32(seq.s, seq.d); break;
#ifdef USE_STRATIFIED_SAMPLES
        case StratifiedSampleGenerator::TYPEID: {
            if(seq.d1 >= SampleSequence::max1d ||
               seq.s-seq.ss >= SampleSequence::maxs)
                ret = seq.rng->next1f();
            else ret = seq.samples1[seq.d1][seq.s-seq.ss];
            seq.d1 += 1;
        } break;
#endif
        case ParametricFastSobolSampleGenerator::TYPEID: {
            ParametricFastSobolSampleGenerator* gen = (ParametricFastSobolSampleGenerator*)seq.generator;
            if(seq.s >= gen->maxs) ret = seq.rng->next1f();
            else {
                unsigned int i = lookupFastSobolSampleGenerator(gen, seq.s, seq.i, seq.j);
                ret = (float)qmcSobol32(i, seq.d);
            }
        } break;
        default: error("sequence type unknown");
    }
    seq.d += 1;
    return ret;
}

vec2f next2f(SampleSequence& seq) {
    vec2f ret;
    switch (seq.generator->type) {
        case CenterSampleGenerator::TYPEID: ret = half2f; break;
        case RandomSampleGenerator::TYPEID: ret = seq.rng->next2f(); break;
        case PixelHaltonSampleGenerator::TYPEID: ret = vec2f((float)qmcHalton(seq.s, seq.d),(float)qmcHalton(seq.s, seq.d+1)); break;
        case PixelSobolSampleGenerator::TYPEID: ret = vec2f((float)qmcSobol32(seq.s, seq.d),(float)qmcSobol32(seq.s, seq.d+1)); break;
#ifdef USE_STRATIFIED_SAMPLES
        case StratifiedSampleGenerator::TYPEID: {
            if(seq.d2 >= SampleSequence::max1d ||
               seq.s-seq.ss >= SampleSequence::maxs)
                ret = seq.rng->next2f();
            else ret = seq.samples2[seq.d2][seq.s-seq.ss];
            seq.d2 += 1;
        } break;
#endif
        case ParametricFastSobolSampleGenerator::TYPEID: {
            ParametricFastSobolSampleGenerator* gen = (ParametricFastSobolSampleGenerator*)seq.generator;
            if(seq.s >= gen->maxs) ret = seq.rng->next2f();
            else {
                unsigned int i = lookupFastSobolSampleGenerator(gen, seq.s, seq.i, seq.j);
                ret = vec2f((float)qmcSobol32(i, seq.d),(float)qmcSobol32(i, seq.d+1));
                if(seq.d == 0) {
                    ret.x = ret.x * gen->r - seq.i;
                    ret.y = ret.y * gen->r - seq.j;
                }
            }
        } break;
        default: error("sequence type unknown");
    }
    seq.d += 2;
    return ret;
}

vec2f nextPixel2f(SampleSequence& seq) {
    error_if_not(seq.d == 0, "wrong sampling dimension");
    return next2f(seq);
}

