#ifndef RENDER_OPTS_H
#define RENDER_OPTS_H

#include <common/json.h>
#include "integration.h"

TraceDirectMode parseDirectMode(const jsonValue& json);
jsonValue printDirectMode(TraceDirectMode dm);

ImageSamplingOpts parseImageSamplingOpts(const jsonObject& json);
jsonObject printImageSamplingOpts(const ImageSamplingOpts& opts);

GeneralOpts parseGeneralOpts(const jsonObject& json);
jsonObject printGeneralOpts(const GeneralOpts& opts);

int parseSampleGenerator(const std::string &type);
std::string printSampleGenerator(int type);

#endif
