#ifndef RENDER_INTEGRATION_H
#define RENDER_INTEGRATION_H

#include <scene/scene.h>
#include "sequence.h"
#include "sampling.h"
#include "options.h"
#include <tbb/tbb.h>
#ifdef _WIN32
#undef near
#undef far
#endif

struct Accelerator;
enum TraceDirectMode {
    TDM_ALL = 1,
    TDM_ALL_MIS = 2,
    TDM_ALL_BRDF = 3,
    TDM_UNIFORM = 4,
    TDM_UNIFORM_MIS = 5,
    TDM_UNIFORM_BRDF = 6
};

struct GeneralOpts{
    GeneralOpts() {
        width = 1024u;
        height = 1024u;
        parallel = true;
    }
    uint    width;
    uint    height;
    bool    parallel;
};

struct ImageSamplingOpts {
    uint                    batchsamples;
    uint                    maxsamples;
    int                     generatortype;
    uint                    blocksize;
    ImageSamplingOpts() {
        batchsamples = 4u;
        maxsamples = 256u;
        generatortype = RandomSampleGenerator::TYPEID;
        blocksize = 64u;
    }
};


struct ImageSamplingState {
    // result
    Image<vec3f>      image;              // current rendering
    Image<vec3f>      accimage;           // accumulator image
    Image<float>      accweight;          // accumulator weights

    // per-pixel state
    Image<int>        samples;            // per-pixel sample number
    Image<Rng>        rngs;               // per-pixel random number generators

    // sequence generator
    SampleGenerator*    generator;          // sequence generator

    ImageSamplingState() { generator = 0; }
    ~ImageSamplingState() { if(generator) delete generator; }
};

void initImageSamplingState(ImageSamplingState& state, const ImageSamplingOpts& opts,
                            int width, int height);

bool traceRay(const ray3f& ray, bool resolveEmission, bool resolveEnvironment,
              const std::vector<Light*> &lights, const Accelerator *accelerator,
              frame3f& f, vec3f& wo, vec3f& le, ReflectanceSample& rs);

vec3f evalEnvironment(const ray3f& ray, const std::vector<Light*>& lights);

vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                 const vec2f& ls, const Light* light, const Accelerator* accelerator);

vec3f evalDirectMis(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                    const vec2f& ls, float bls, const vec2f& bas,
                    const Light* light, const Accelerator* accelerator);

vec3f evalDirect(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo,
                 SampleSequence& seq, TraceDirectMode mode,
                 const std::vector<Light*>& lights, const Accelerator* accelerator);


// template implementation ---------------------------

template<typename T>
inline void samplePixel(int i, int j, const Scene* scene, const T& func,
                        ImageSamplingState& state, const ImageSamplingOpts& opts) {
    SampleSequence seq;
    initSequence(seq, state.generator, state.rngs.at(i,j),
                 i, j, state.samples.at(i,j),opts.batchsamples);
    for(int s = 0; s < opts.batchsamples; s ++) {
        vec2f rs = next2f(seq);
        vec2f is = vec2f((i + rs.x)/state.image.width(),
                         (j + rs.y)/state.image.height());
        ray3f ray = sampleCamera(scene->cameras[0], is);
        vec3f l = func(ray,seq);
        state.samples.at(i,j) += 1;
        state.accimage.at(i,j) += l;
        state.accweight.at(i,j) += 1;
        nextSample(seq);
    }
    state.image.at(i,j) = state.accimage.at(i,j) / state.accweight.at(i,j);
}

template<typename T>
inline void sampleImage(const Scene* scene, const T& func,
                        ImageSamplingState& state, const ImageSamplingOpts& opts,
                        const GeneralOpts &gopts) {
    const int width = gopts.width;
    const int height = gopts.height;
    if(gopts.parallel) {
        auto samplePixelFunc = [scene, &state, &opts, &func]
        (const tbb::blocked_range2d<int>& range) {
            for(int j = range.cols().begin(); j < range.cols().end(); j ++) {
                for(int i = range.rows().begin(); i < range.rows().end(); i ++) {
                    samplePixel(i,j,scene,func,state,opts);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range2d<int>(0, width, opts.blocksize,
                                                    0, height, opts.blocksize),
                          samplePixelFunc,
                          tbb::simple_partitioner());
    } else {
        for(int j = 0; j < height; j ++) {
            for(int i = 0; i < width; i ++) {
                samplePixel(i, j, scene, func, state, opts);
            }
        }
    }
}

template<typename TR>
void renderImageFunc(TR* render, const ImageSamplingOpts &opts) {
    int maxsamples = opts.maxsamples;
    int batchsamples = opts.batchsamples;
    int passes = maxsamples / batchsamples;
    for(int p = 0; p < passes; p ++) {
        render->tracePass();
    }
}

#endif
