#ifndef RENDER_SAMPLING_H
#define RENDER_SAMPLING_H

#include <scene/scene.h>
#include <scene/material.h>
#include <common/montecarlo.h>
#include <common/distribution.h>

ray3f sampleCamera(const Camera* camera, const vec2f& is);
ray3f sampleCamera(const Camera* camera, const vec2f& is, const vec2f& ls);

struct ShapeSample {
    frame3f frame;
    float area;
};
inline bool isvalid(const ShapeSample& ss) { return ss.area != 0; }
const ShapeSample invalidShapeSample = { defaultframe3f, 0.0f };
ShapeSample sampleShape(const Shape* shape, const vec2f& s);

struct LightSample {
    Light* light;
    float pdf;
};
inline bool isvalid(const LightSample& ss) { return ss.light != 0 && ss.pdf != 0; }
const LightSample invalidLightSample = { 0, 0.0f };
LightSample sampleLight(const std::vector<Light*>& lights, float s);

struct ShadowSample {
    vec3f le;
    ray3f ray;
    float pdf;
};
inline bool isvalid(const ShadowSample& ss) { return !ss.le.iszero() && ss.pdf != 0; }
const ShadowSample invalidShadowSample = { zero3f, ray3f(zero3f, z3f), 0.0f };
ShadowSample sampleShadow(const Light* light, const vec3f& p, const vec2f& s);
ShadowSample sampleShadow(const Light* light, const vec3f& p, const vec3f& wi);
float sampleShadowPdf(const Light* light, const vec3f& p, const vec3f& wi);

struct PhotonSample {
    vec3f le;
    vec3f normal;
    ray3f ray;
    float pdf;
};
inline bool isvalid(const PhotonSample& ps) { return !ps.le.iszero() && ps.pdf != 0; }
const PhotonSample invalidPhotonSample = { zero3f, z3f, ray3f(zero3f, z3f), 0.0f };
PhotonSample samplePhoton(const Light* light, const vec2f &ss, const vec2f &sa, const vec3f &sceneCenter, float sceneRadius);

// TODO: emission should be polymorphic
struct EmissionSample {
    int type;
    vec3f le;
};
inline bool isvalid(const EmissionSample& es) { return es.type != 0; }
const EmissionSample invalidEmissionSample = { 0, zero3f };
EmissionSample sampleEmission(const Material* m, const vec2f& uv);
vec3f evalEmission(const EmissionSample& es, const frame3f& f, const vec3f& wo);

struct ReflectanceSample {
	int type;
	vec3f rhod;
    vec3f rhos;
    float n;
    int diffuseMode, specularMode;
};
inline bool isvalid(const ReflectanceSample& rs) { return rs.type > 0; }
const ReflectanceSample invalidReflectanceSample = { 0, zero3f, zero3f, 0, 0, 0 };
ReflectanceSample sampleReflectance(const Material* m, const vec2f& uv);
vec3f evalReflectance(const ReflectanceSample& rs, const frame3f& f, const vec3f& wi, const vec3f& wo);
vec3f evalReflectance(const ReflectanceSample& rs, const vec3f& wil, const vec3f& wol);
ReflectanceSample reflectanceToLambert(const ReflectanceSample& rs);
inline bool isLambert(const ReflectanceSample &rs) { return rs.type == LambertMaterial::TYPEID; }

struct ScatteringSample {
    vec3f bsdf;
    ray3f ray;
    float pdf;
};
inline bool isvalid(const ScatteringSample& ss) { return (ss.pdf > 0) && (!ss.bsdf.iszero()); }
const ScatteringSample invalidScatteringSample = { zero3f, ray3f(zero3f, z3f), 0 };
ScatteringSample sampleScattering(const ReflectanceSample& rs, const frame3f& f,
                                  const vec3f& wo, float ls, const vec2f& as);
float sampleScatteringPdf(const ReflectanceSample& rs, const frame3f& f, const vec3f& wo, const vec3f& wi);
float sampleScatteringPdf(const ReflectanceSample& rs, const vec3f& wol, const vec3f& wil);

vec3f sampleSpatialVariation(const vec3f& v, Texture* txt, const vec2f& uv);
float sampleSpatialVariation(float v, Texture* txt, const vec2f& uv);
vec3f sampleEnvironment(const Light* light, const vec3f& wo);

void initLightSampling(const std::vector<Light*>& lights);
bool isDelta(const Light* light);

vec3f sampleImage(const Image<vec3f> &image, const vec2f &uv);
vec3f sampleTexture(const Texture* txt, const vec2f& uv);

struct EnvironmentLightAux : TraceAux {
    Distribution2D<float> dist;
};

float shapeArea(const Shape* shape);
#endif
