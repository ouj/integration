#include "opts.h"
#include <common/json.h>
using std::string;
static const sbimap<int> generatortype_b = {
    { "center", CenterSampleGenerator::TYPEID },
    { "random", RandomSampleGenerator::TYPEID },
#ifdef USE_STRATIFIED_SAMPLES
    { "stratified", StratifiedSampleGenerator::TYPEID },
#endif
    { "pixelhalton", PixelHaltonSampleGenerator::TYPEID },
    { "pixelsobol", PixelSobolSampleGenerator::TYPEID },
    { "sobol", ParametricFastSobolSampleGenerator::TYPEID}
};

int parseSampleGenerator(const string &type) { return jsonToEnum(type, generatortype_b); }
string printSampleGenerator(int type) { return jsonFromEnum(type, generatortype_b); }

static const sbimap<TraceDirectMode> directmode_b = {
    { "all", TDM_ALL },
    { "all_mis", TDM_ALL_MIS },
    { "all_brdf", TDM_ALL_BRDF },
    { "uniform", TDM_UNIFORM },
    { "uniform_mis", TDM_UNIFORM_MIS },
    { "uniform_brdf", TDM_UNIFORM_BRDF }
};

TraceDirectMode parseDirectMode(const jsonValue& json) { return jsonToEnum(json, directmode_b); }
jsonValue printDirectMode(TraceDirectMode dm) { return jsonFromEnum(dm, directmode_b); }

ImageSamplingOpts parseImageSamplingOpts(const jsonObject& json) {
    ImageSamplingOpts opts;
    opts.batchsamples = jsonGet(json,"batchsamples");
    opts.maxsamples = jsonGet(json,"maxsamples");
    opts.generatortype = parseSampleGenerator(jsonGet(json,"generatortype"));
    opts.blocksize = jsonGet(json,"blocksize");
    return opts;
}

jsonObject printImageSamplingOpts(const ImageSamplingOpts& opts) {
    jsonObject json;
    json["batchsamples"] = opts.batchsamples;
    json["maxsamples"] = opts.maxsamples;
    json["generatortype"] = printSampleGenerator(opts.generatortype);
    json["blocksize"] = opts.blocksize;
    return json;
}

GeneralOpts parseGeneralOpts(const jsonObject& json) {
    GeneralOpts opts;
    opts.width = jsonGet(json,"width");
    opts.height = jsonGet(json,"height");
    opts.parallel = jsonGet(json,"parallel");
    return opts;
}

jsonObject printGeneralOpts(const GeneralOpts& opts) {
    jsonObject json;
    json["width"] = opts.width;
    json["height"] = opts.height;
    json["parallel"] = opts.parallel;
    return json;
}








