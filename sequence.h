#ifndef RENDER_SEQUENCE_H
#define RENDER_SEQUENCE_H

#include <common/vec.h>
#include <common/array.h>
#include <common/random.h>
#include <common/montecarlo.h>
#include <common/json.h>

struct SampleGenerator {
    SampleGenerator(int t) : type(t) {}
    int type;
};

// center for debugging
struct CenterSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000000;
    CenterSampleGenerator() : SampleGenerator(TYPEID) {}
};

// fully random
struct RandomSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000001;
    RandomSampleGenerator() : SampleGenerator(TYPEID) {}
};

#ifdef USE_STRATIFIED_SAMPLES
// stratified sampling
struct StratifiedSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000002;
    StratifiedSampleGenerator() : SampleGenerator(TYPEID) {}
};
#endif

// same sequence for each pixel
struct PixelHaltonSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000011;
    PixelHaltonSampleGenerator() : SampleGenerator(TYPEID) {}
};

// same sequence for each pixel
struct PixelSobolSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000012;
    PixelSobolSampleGenerator() : SampleGenerator(TYPEID) {}
};

// fast parametric sobol generator
// from Keller et al. talks and brevetto
struct ParametricFastSobolSampleGenerator : SampleGenerator {
    static constexpr int TYPEID = 5000016;
    static constexpr int maxs = 4096;

    typedef unsigned int uint;

    int m;                  // number of bits for the image square
    int r;                  // length of the side of the image square
    Image<uint> c0Lower;  // lookup tables, index by sample number and index
    Image<uint> c1Lower;
    Image<uint> c0Upper;
    Image<uint> c1Upper;
    Image<uint> c0LowerInverse;
    Image<uint> c1UpperInverse;

    ParametricFastSobolSampleGenerator() : SampleGenerator(TYPEID) {}
};
void initParametricFastSobolSampleGenerator(
            ParametricFastSobolSampleGenerator* generator,
            int w, int h, int maxs);

SampleGenerator* initGenerator(int type, int w, int h, int s);

struct SampleSequence {
    static constexpr int maxs = 16;
    static constexpr int max1d = 12;
    static constexpr int max2d = 12;

    const SampleGenerator* generator;
    int i;                          // pixel coordinate
    int j;                          // pixel coordinate
    int d;                          // current dimension
    int s;                          // current sample number for this pixel
    int ss;                         // current sample number in this batch
    int ns;                         // number of samples for this batch
    Rng* rng;                       // random number generator -> one per thread

#ifdef USE_STRATIFIED_SAMPLES
    int d1, d2;                     // precomputed values
    std::array<std::array<float,maxs>,max1d> samples1;    // precomputed values
    std::array<std::array<vec2f,maxs>,max2d> samples2;    // precomputed values
#endif
};

void initSequence(SampleSequence& seq, const SampleGenerator* generator,
                  Rng& rng, int i, int j, int s, int ns);
float next1f(SampleSequence& seq);
vec2f next2f(SampleSequence& seq);
vec2f nextPixel2f(SampleSequence& seq);
void nextSample(SampleSequence& seq);

#endif
